import subprocess

with open("hosts_and_ports.txt") as hp_fh:
    hp_contents = hp_fh.readlines()
    for hp_pair in hp_contents:
        with open("commands.txt") as fh:
                ip_en_poort = (hp_pair.split(":"))
                poort = ip_en_poort[1]                   
                completed = subprocess.run("ssh ubuntussh@127.0.0.1 -p "+ poort, capture_output=True, text=True, shell=True, stdin=fh)
