import python_spam_regel_functie

def file_is_spam(uitkomst):
    with open(uitkomst) as waar_of_nietwaar:
        is_spam = False
        for zin in waar_of_nietwaar.readlines():
            is_spam = is_spam or python_spam_regel_functie.line_is_spam(zin.strip())
    return is_spam
