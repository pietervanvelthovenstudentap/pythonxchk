##def bestaat_uit_bytes(ip_adres):
##    antwoord = ("True")
##    elementen = ip_adres.split(".")
##    elementen = (int(i) for i in elementen)
##    for element in elementen:
##        if element < 0 or element > 255:
##            antwoord=("False")
##            break
##    print(antwoord)
##mijn oplossing schijnt ook te werken

def bestaat_uit_bytes(s):
    elements = s.split(".")
    result = True
    for elem in elements:
        num = int(elem)
        if not (num >= 0 and num <= 255):
            result = False
    return result


