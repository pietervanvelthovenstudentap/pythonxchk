import sys
def ask_for_number_sequence_ip():
    getallen = input("Wat is het IP-adres?\n")
    ip_adres =[int(getal) for (getal) in getallen.split(".")]
    return ip_adres

def ask_for_number_sequence_subnet():
    getallen = input("Wat is het subnetmasker?\n")
    subnet = [int(getal) for (getal) in getallen.split(".")]
    return subnet

def is_valid_ip_address(numberlist):
    resultaat = True
    for number in numberlist:
        if not(number >= 0 and number <=255) or len(numberlist)!=4:
            resultaat = False
    if resultaat == True:
        print ("IP-adres is geldig.")
    else:
        print ("IP-adres is ongeldig")
        sys.exit(0)
    return resultaat

def is_valid_netmask(numberlist):
    bytes = []
    resultaat = True
    if len(numberlist) !=4:
        resultaat = False
    else:
        for number in numberlist:
            bytes.append("{:08b}".format(number))
        binary_netmask = bytes[0]+bytes[1]+bytes[2]+bytes[3]
        checking_ones = True
        for number in binary_netmask:
            if number == "1" and checking_ones == True:
                continue
            elif number == "0":
                checking_ones = False
            elif number == "1" and checking_ones == False:
                
                resultaat = False
                break
    if resultaat == True:
        print ("Subnetmasker is geldig.")
    else:
        print ("Subnetmasker is ongeldig")
        sys.exit(0)
    return resultaat

def one_bits_in_netmask(numberlist):
    bytes = []
    for number in numberlist:        
        bytes.append("{:08b}".format(number))
    binary_netmask = bytes[0]+bytes[1]+bytes[2]+bytes[3]   
    counter = 0
    for number in binary_netmask:
        if number == "1":
            counter += 1
        if number == "0":
            break
    print ("De lengte van het subnetmaskter is "+str(counter)+".")
    return counter

def apply_network_mask(host_address, netmask):
    ipv4 = []
    for number in host_address:        
        ipv4.append("{:08b}".format(number))
    ipv4_lang = ipv4[0]+ipv4[1]+ipv4[2]+ipv4[3]
    ipv4_lang = int(ipv4_lang,2)
    subnet = []
    for number in netmask:        
        subnet.append("{:08b}".format(number))
    subnet_lang = subnet[0]+subnet[1]+subnet[2]+subnet[3]
    subnet_lang = int(subnet_lang,2)
    network_mask = (f"{ipv4_lang & subnet_lang:032b}")
    byte1 = (network_mask[0:8])
    byte1 = int(byte1,2)
    byte2 = (network_mask[8:16])
    byte2 = int(byte2,2)
    byte3 = (network_mask[16:24])
    byte3 = int(byte3,2)
    byte4 = (network_mask[24:32])
    byte4 = int(byte4,2)
    network_mask = [byte1,byte2,byte3,byte4]
    print ("Het adres van het subnet is "+str(network_mask[0])+"."+str(network_mask[1])+"."+str(network_mask[2])+"."+str(network_mask[3])+".")
    return network_mask

def netmask_to_wildcard_mask(netmask):
    wildcard_mask = []
    for netmask_part in netmask:
        wildcard = ""
        
        for bit in f"{netmask_part:08b}":
            if bit == "0":
                wildcard += "1"
            else:
                wildcard += "0"    
        wildcard_mask.append(int(wildcard,2))
    print ("Het wildcardmasker is "+str(wildcard_mask[0])+"."+str(wildcard_mask[1])+"."+str(wildcard_mask[2])+"."+str(wildcard_mask[3])+".")
    return wildcard_mask
    
def get_broadcast_address(network_address, wildcard_mask):
    broadcast_address = ""
    network_addresses = []
    for network_address_part in network_address:        
        network_addresses.append("{:08b}".format(network_address_part))
    network_addresses_lang = network_addresses[0]+network_addresses[1]+network_addresses[2]+network_addresses[3]
    network_addresses_lang = int(network_addresses_lang,2)
    wildcard_masks = []
    for wildcard_mask_part in wildcard_mask:        
        wildcard_masks.append("{:08b}".format(wildcard_mask_part))
    wildcard_masks_lang = wildcard_masks[0]+wildcard_masks[1]+wildcard_masks[2]+wildcard_masks[3]
    wildcard_masks_lang = int(wildcard_masks_lang,2)
    broadcast_address = (f"{network_addresses_lang | wildcard_masks_lang:032b}")
    byte1 = (broadcast_address[0:8])
    byte1 = int(byte1,2)
    byte2 = (broadcast_address[8:16])
    byte2 = int(byte2,2)
    byte3 = (broadcast_address[16:24])
    byte3 = int(byte3,2)
    byte4 = (broadcast_address[24:32])
    byte4 = int(byte4,2)
    broadcast_address = (str(byte1)+"."+str(byte2)+"."+str(byte3)+"."+str(byte4))
    print ("Het broadcastadres is "+str(broadcast_address)+".")
    return broadcast_address

def prefix_length_to_max_hosts(subnetmask):
    hosts = 2**(32-subnetmask)-2
    print ("Het maximaal aantal hosts op dit subnet is "+str(hosts)+".")
    return hosts



ip_adres = ask_for_number_sequence_ip()
subnet = ask_for_number_sequence_subnet()
is_valid_ip_address(ip_adres)
is_valid_netmask(subnet)
netmask = one_bits_in_netmask(subnet)
netwerkadres = apply_network_mask(ip_adres,subnet)
wildcard = netmask_to_wildcard_mask(subnet)
get_broadcast_address(netwerkadres,wildcard)
prefix_length_to_max_hosts(netmask)

            
        








    
