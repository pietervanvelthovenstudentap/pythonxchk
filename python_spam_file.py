import python_spam_regel_functie
def file_is_spam():
    pad = input("Welke file moet gecontroleerd worden?\n")
    with open(pad) as bestand:
        is_spam = False
        for zin in bestand.readlines():
            is_spam = is_spam or python_spam_regel_functie.line_is_spam(zin.strip())
    if is_spam:
       print("Deze file bevat spam!")
    else:
        print("Deze file is in orde!")


        

